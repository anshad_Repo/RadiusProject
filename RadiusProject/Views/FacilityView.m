//
//  FacilityView.m
//  RadiusProject
//
//  Created by AMK on 11/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "FacilityView.h"
#import "RadiusFacilityTableViewCell.h"

@interface FacilityView()

@property (nonatomic, assign) BOOL didLayoutConstraints;

@end

@implementation FacilityView

#pragma mark - Getters

- (UITableView *)facilityTableView{
    
    if(!_facilityTableView){
        
        _facilityTableView = [UITableView new];
        [_facilityTableView registerNib:[UINib nibWithNibName:@"RadiusFacilityTableViewCell"  bundle:nil] forCellReuseIdentifier:@"Cell"];
    }
    return _facilityTableView;
}

#pragma mark - Views

+ (BOOL)requiresConstraintBasedLayout{
    
    return YES;
}

- (void)addSubViews{
    
    [self addSubview:self.facilityTableView];
}

- (void)updateConstraints{
    
    if(!_didLayoutConstraints){
        [self addSubViews];
        [self.facilityTableView autoPinEdgesToSuperviewEdges];
    }
    [super updateConstraints];
}


#pragma mark - View

- (void)reloadView{
    
    [self.facilityTableView reloadData];
}

@end
